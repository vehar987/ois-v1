import java.util.*;

public class Gravitacija {
    public static final double C_CONST = 6.674*Math.pow(10,-11);
    public static final double M_CONST = 5.927*Math.pow(10,24);
    public static final double R_CONST = 6.371*Math.pow(10,6);
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int vnesenaVrednost = sc.nextInt();
        System.out.println("OIS je zakon!");
        izpis(vnesenaVrednost, pospesek(vnesenaVrednost));
        
    }
    private static double pospesek(int v){
      
        return ((C_CONST*M_CONST)/Math.pow((R_CONST+v),2));
    }

    private static void izpis(int v, double a){
        
        System.out.println("Za nadmorsko visino "+v+" m, je gravitacijski pospesek "+a);
        
    }
}